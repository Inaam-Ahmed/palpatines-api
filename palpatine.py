import requests
import pandas as pd
import random
import json
import datetime as dt

def reader(filename):
    '''
    Read the file specified and return the list containing all the lines as elements.
    '''
    try:
        with open(filename,'r') as f:
            lines = f.readlines()
            return [line.strip('\n') for line in lines]
    except:
        print('Unable to read the file specified!')
        
        
def decrypt_citizens(url, headers, body):
    
    '''
    Decrypt the list of string formatted data provided as \'body\' using external API
    '''

    decrypted_citizens = [] 
    
    try:
        response = requests.post(url, headers = headers, json = body)
        for i in response.json():
            decrypted_citizens.append(json.loads(i))
        return decrypted_citizens
    except:
        print(f'request to the specified URL:{url} is failed!')

def writer(filename, *info_list):
    '''
    Write the decrypted \'info\' list of dictionaries to text file specified.
    '''
    for info in info_list:
        if info:
            try:
                with open(filename, 'w', encoding='utf-8') as f: 
                    for key in info.keys():
                        f.write('%s\n' % (key))
                        for value in info[key]:
                            f.write('-%s\n'% value)
                        f.write('\n\n')
                    f.close()
            except:
                print('Unable to write the file specified!')
                
def fetch_name(url):
    '''
    Fetch the name from external API (SWAPI) using homeworld URL.
    '''
    try:
        return requests.get(url).json()['name']
    except:
        return ''
    
    
SECRET_FILE_NAME = 'super-secret-data.txt'
OUTPUT_FILE_NAME = 'citizens-super-secret-info.txt'
MAX_CITIZENS = 200 # efficiently use the API rate limit.

def main():

    # Decryption API Parameters
    headers = {'x-api-key' : 'ZH8KOCTJ6F7dAa1qZO62S3KwKMMsjrSt1xS5LhUS', 'charset' : 'utf-8'}
    url = 'https://txje3ik1cb.execute-api.us-east-1.amazonaws.com/prod/decrypt'
    body = reader(filename = SECRET_FILE_NAME)

    if len(body) > MAX_CITIZENS:
        body = random.sample(set(body), MAX_CITIZENS)

    print(dt.datetime.now(), 'Decrypting Citizens Info!')
    decrypted_citizens = decrypt_citizens(url, headers, body)

    if decrypted_citizens:

        citizens_df = pd.DataFrame(decrypted_citizens)[['name','homeworld']]
        citizens_df = citizens_df.drop_duplicates(subset = ['name'])

        print(dt.datetime.now(), 'Fetching names using homeworld URL!')
        citizens_df['homeworld_name'] = citizens_df['homeworld'].apply(fetch_name)

        print(dt.datetime.now(), 'Writing the output file!')
        writer(OUTPUT_FILE_NAME, *[
            citizens_df[citizens_df['homeworld_name'] != ''].groupby('homeworld_name')['name'].unique().to_dict(),
            citizens_df[citizens_df['homeworld_name'] == ''].groupby('homeworld')['name'].unique().to_dict()
        ])

        print(dt.datetime.now(), 'Done')
    else:
        print('Sorry, Mr. Supreme Chancellor there is no sufficient info attained to produce actionable results!')
        

if __name__ == "__main__":
   main()
